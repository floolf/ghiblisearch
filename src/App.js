import React, { Component } from 'react';
import './App.css';
import HeaderGhibli from "./compoments/Header";
import ClassesGhibli from "./compoments/Classes";
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';

class App extends Component {
  render() {
    return (
      <div className="App">
          <HeaderGhibli/>

        <div className="App-intro">
            <ClassesGhibli/>

        </div>
      </div>
    );
  }
}
export default App;


