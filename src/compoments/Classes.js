import React from "react";
import HeaderGhibli from "./Header";

class ClassesGhibli extends React.Component{
    render() {
        return (
            <div id={"card"}>
                <a href="#" id={"a-lieux"}>
                    <div className="card-body" id={"Lieux"}>
                        <div id="lieux-title">
                            <h3 className="card-title">Lieux</h3>
                        </div>
                    </div>
                </a>

                <a href="#" id={"a-especes"}>
                    <div className="card-body" id={"Especes"}>
                        <div id="especes-title">
                            <h3 className="card-title">Especes</h3>
                        </div>
                    </div>
                </a>

                <a href="#" id={"a-perso"}>
                    <div className="card-body" id={"Personnages"}>
                        <div id="perso-title">
                            <h3 className="card-title">Personnages</h3>
                        </div>
                    </div>
                </a>

                <a href="#" id={"a-vehicules"}>
                    <div className="card-body" id={"Vehicules"}>
                        <div id="vehicules-title">
                            <h3 className="card-title">Vehicules</h3>
                        </div>
                    </div>
                </a>
            </div>
        );
    }
}

export default ClassesGhibli;