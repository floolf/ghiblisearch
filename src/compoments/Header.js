import React from 'react';

class HeaderGhibli extends React.Component{

    render() {
        return(
            <div className="navbar navbar-expand-lg">
                <div className="input-group mb-3">
                    <input className="form-control mr-sm-2" id="search" type="search" placeholder="Search" aria-label="Search"/>
                        <div className="input-group-append">
                            <button className="btn btn-outline-success my-2 my-sm-0" id="btnSearch" type="submit" onSubmit={(event => this.Search)}/>
                        </div>
                </div>
            </div>

        )
    }
}

export default HeaderGhibli;